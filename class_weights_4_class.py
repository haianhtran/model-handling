import cv2
import os
import numba

def parse_code(l):
    '''Function to parse lines in a text file, returns separated elements (label codes and names in this case)
    '''
    if len(l.strip().split("\t")) == 2:
        a, b = l.strip().split("\t")
        return tuple(int(i) for i in a.split(' ')), b
    else:
        a, b, c = l.strip().split("\t")
        return tuple(int(i) for i in a.split(' ')), c

@numba.njit(fastmath=True)
def matrix(total_road,total_line,total_background,total_sign,image):
    for j in range(0,480):
        for k in range(0,640):
            b,g,r = image[j][k]
            if (r,g,b) == (128,0,0):
                total_road += 1
            if (r,g,b) == (192,128,128):
                total_line += 1
            if (r,g,b) == (0,0,0):
                total_background +=1
            if (r,g,b) == (102,102,102):
                total_sign +=1

    return total_road,total_line,total_background,total_sign

def calc_weights(masks_folder):
    """Calculate class weights according to classes distribution in a dataset"""
    images_list = os.listdir(masks_folder)
    total_road = 0
    total_line = 0
    total_background = 0
    total_sign = 0
    for i in range(len(images_list)):
        print("Processing---------",i,"/",len(images_list))
        image = cv2.imread(masks_folder + '/' + images_list[i])
        total_road,total_line,total_background,total_sign = matrix(total_road,total_line,total_background,total_sign,image)
        #b,g,r = image[373][145]
        #print(b,g,r)

    total = len(images_list) * 480 * 640
    print("------------------------------------")
    print("Weight of Road: ", total_road / total)
    print("Weight of Line: ", total_line / total)
    print("Weight of BG: ", total_background / total)
    print("Weight of Sign: ", total_sign / total)



if __name__ == '__main__':
    img_dir = '/home/dylan/Desktop/Goodgame_Training/data/Data_4_class_Non_Lib/train_masks/train'
    
    DATA_PATH = '/home/dylan/Desktop/Goodgame_Training/data/Data_4_class_Non_Lib/'

    label_codes, label_names = zip(*[parse_code(l) for l in open(DATA_PATH+"label_colors.txt")])
    
    label_codes, label_names = list(label_codes), list(label_names)

    code2id = {v:k for k,v in enumerate(label_codes)}
    id2code = {k:v for k,v in enumerate(label_codes)}

    name2id = {v:k for k,v in enumerate(label_names)}
    id2name = {k:v for k,v in enumerate(label_names)}

    print(id2code,id2name)

    calc_weights(img_dir)
