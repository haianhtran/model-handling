from keras import backend as K
# This line must be executed before loading Keras model.
K.set_learning_phase(0)
import tensorflow as tf
from denseunet.unet_densenet import unet_densenet # import denseunet
import cv2
import segmentation_models as sm
from segmentation_models import PSPNet
from segmentation_models.metrics import IOUScore
from segmentation_models.losses import JaccardLoss,CategoricalFocalLoss,DiceLoss,CategoricalCELoss

# import numpy as np
# import matplotlib.pyplot as plt
# from keras.models import Sequential
# from keras.layers import Dense
# from keras.optimizers import Adam
# from keras.utils.np_utils import to_categorical
# from keras.layers import Dropout, Flatten
# from keras.layers.convolutional import Conv2D, MaxPooling2D
# import cv2
# from sklearn.model_selection import train_test_split
# import pickle
# import os
# import pandas as pd
# import random
# from keras.preprocessing.image import ImageDataGenerator
# from keras.callbacks import ModelCheckpoint, EarlyStopping,ReduceLROnPlateau



def freeze_session(session, keep_var_names=None, output_names=None, clear_devices=True):
    """
    Freezes the state of a session into a pruned computation graph.

    Creates a new computation graph where variable nodes are replaced by
    constants taking their current value in the session. The new graph will be
    pruned so subgraphs that are not necessary to compute the requested
    outputs are removed.
    @param session The TensorFlow session to be frozen.
    @param keep_var_names A list of variable names that should not be frozen,
                          or None to freeze all the variables in the graph.
    @param output_names Names of the relevant graph outputs.
    @param clear_devices Remove the device directives from the graph for better portability.
    @return The frozen graph definition.
    """
    from tensorflow.python.framework.graph_util import convert_variables_to_constants
    graph = session.graph
    with graph.as_default():
        freeze_var_names = list(set(v.op.name for v in tf.global_variables()).difference(keep_var_names or []))
        output_names = output_names or []
        output_names += [v.op.name for v in tf.global_variables()]
        # Graph -> GraphDef ProtoBuf
        input_graph_def = graph.as_graph_def()
        if clear_devices:
            for node in input_graph_def.node:
                node.device = ""
        frozen_graph = convert_variables_to_constants(session, input_graph_def,
                                                      output_names, freeze_var_names)
        return frozen_graph

# model = unet_densenet((144,144,3),num_class=9) # Call denseNet model

model = PSPNet(backbone_name='mobilenetv2',
                    input_shape=(144, 144, 3),
                    classes=4,
                    activation='softmax',
                    weights=None,
                    encoder_weights='imagenet',
                    encoder_freeze=True,
                    downsample_factor=4,
                    psp_conv_filters=512, 
                    psp_pooling_type='avg',
                    psp_use_batchnorm=True,
                    psp_dropout=None)

# noOfClasses = 6 
# ############################### CONVOLUTION NEURAL NETWORK MODEL
# def myModel():
#     no_Of_Filters=32
#     size_of_Filter=(5,5) # THIS IS THE KERNEL THAT MOVE AROUND THE IMAGE TO GET THE FEATURES.
#                          # THIS WOULD REMOVE 2 PIXELS FROM EACH BORDER WHEN USING 32 32 IMAGE
#     size_of_Filter2=(3,3)
#     size_of_pool=(2,2)  # SCALE DOWN ALL FEATURE MAP TO GERNALIZE MORE, TO REDUCE OVERFITTING
#     no_Of_Nodes = 512   # NO. OF NODES IN HIDDEN LAYERS
#     model= Sequential()
#     model.add((Conv2D(no_Of_Filters,size_of_Filter,input_shape=(32,32,1),activation='relu')))  # ADDING MORE CONVOLUTION LAYERS = LESS FEATURES BUT CAN CAUSE ACCURACY TO INCREASE
#     model.add((Conv2D(no_Of_Filters, size_of_Filter, activation='relu')))
#     model.add(MaxPooling2D(pool_size=size_of_pool)) # DOES NOT EFFECT THE DEPTH/NO OF FILTERS
#     model.add((Conv2D(no_Of_Filters//2, size_of_Filter2,activation='relu')))
#     model.add((Conv2D(no_Of_Filters // 2, size_of_Filter2, activation='relu')))
#     model.add(MaxPooling2D(pool_size=size_of_pool))
#     model.add(Dropout(0.5))
#     model.add(Flatten())
#     model.add(Dense(no_Of_Nodes,activation='relu'))
#     model.add(Dropout(0.5)) # INPUTS NODES TO DROP WITH EACH UPDATE 1 ALL 0 NONE
#     model.add(Dense(noOfClasses,activation='softmax')) # OUTPUT LAYER
#     # COMPILE MODEL
#     model.compile(Adam(lr=0.001),loss='categorical_crossentropy',metrics=['accuracy'])
#     return model
# ############################### TRAIN
# model = myModel()
# print(model.summary())

model.load_weights('/home/dylan/Desktop/Goodgame_Training/model/Model_4_class_Test_1_epoch-175_loss-0.7673_val_loss-0.7632.h5')

frozen_graph = freeze_session(K.get_session(),
                              output_names=[out.op.name for out in model.outputs])
tf.train.write_graph(frozen_graph, "model_pb", "Model_4_class_Test_1_epoch-175_loss-0.7673_val_loss-0.7632.pb", as_text=False)

print("Input Model:  ",model.inputs)
print("Output Model:  ",model.outputs)

# Khi em gui model em phai gui file .pb va model.inputs va model.outputs