import numpy as np
import matplotlib.pyplot as plt
#%matplotlib inline
import os
import random
import re
from PIL import Image


import numpy as np
import matplotlib.pyplot as plt
from pylab import *
import os
import sys
from tensorflow.keras.models import Model
from tensorflow.keras.regularizers import l2
#from tensorflow.keras.layers import *
#from tensorflow.keras.engine import Layer
#from tensorflow.keras.applications.vgg16 import *
#from tensorflow.keras.models import *
#from tensorflow.keras.applications.imagenet_utils import _obtain_input_shape
import tensorflow.keras.backend as K
import tensorflow as tf
from keras.optimizers import Adam
from tensorflow.keras.layers import Convolution2D, ZeroPadding2D, MaxPooling2D, Cropping2D, Conv2D
from tensorflow.keras.layers import Input, Add, Dropout, Permute, add
from tensorflow.compat.v1.layers import conv2d_transpose
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping, ReduceLROnPlateau

from denseunet.unet_densenet import unet_densenet # import denseunet
from bisenet.bisenet import bisenet # Import bisenet model
import cv2
import segmentation_models as sm
from segmentation_models import PSPNet,FPN,Unet, Linknet
from segmentation_models.metrics import IOUScore, Precision,Recall
from segmentation_models.losses import JaccardLoss,CategoricalFocalLoss,DiceLoss,CategoricalCELoss

import cldice 
#from keras_radam import RAdam
from matplotlib import pyplot


'''
In our setup, we:
- created a data/ folder
- created CDS folder which contains all folders
- created 4 folders train_frames train_masks val_frames val_masks
- created train and val subfolders inside 4 folders
- put the pictures in data/CDS/train_frames/train/
- put the label pictures in data/train_masks/train/
In summary, this is our directory structure:
```
    data/
        CDS/
            label_colors.txt
            train_frames/
                train/
                    frame1.png # .png format
            train_masks/
                train/
                    frame1_L.png # _L.png format
            val_frames/
                val/
                    frame2.png # .png format
            val_masks/
                val/
                    frame2_L.png # _L.png format
```
'''

#Function to parse the file "label_colors.txt" which contains the class definitions

def parse_code(l):
    '''Function to parse lines in a text file, returns separated elements (label codes and names in this case)
    '''
    if len(l.strip().split("\t")) == 2:
        a, b = l.strip().split("\t")
        return tuple(int(i) for i in a.split(' ')), b
    else:
        a, b, c = l.strip().split("\t")
        return tuple(int(i) for i in a.split(' ')), c
    
def rgb_to_onehot(rgb_image, colormap):
    '''Function to one hot encode RGB mask labels
        Inputs: 
            rgb_image - image matrix (eg. 144 x 144 x 3 dimension numpy ndarray)
            colormap - dictionary of color to label id
        Output: One hot encoded image of dimensions (height x width x num_classes) where num_classes = len(colormap)
    '''
    #print("RGB: ",rgb_image.shape)
    num_classes = len(colormap)
    shape = rgb_image.shape[:2]+(num_classes,)
    encoded_image = np.zeros( shape, dtype=np.int8 )
    for i, cls in enumerate(colormap):
        encoded_image[:,:,i] = np.all(rgb_image.reshape( (-1,3) ) == colormap[i], axis=1).reshape(shape[:2])
    #print(encoded_image.shape)
    return encoded_image


def onehot_to_rgb(onehot, colormap):
    '''Function to decode encoded mask labels
        Inputs: 
            onehot - one hot encoded image matrix (height x width x num_classes)
            colormap - dictionary of color to label id
        Output: Decoded RGB image (height x width x 3) 
    '''
    single_layer = np.argmax(onehot, axis=-1)
    output = np.zeros( onehot.shape[:2]+(3,) )
    for k in colormap.keys():
        output[single_layer==k] = colormap[k]
    return np.uint8(output)

def TrainAugmentGenerator(seed = 909, batch_size = 16):
    '''Train Image data generator
        Inputs: 
            seed - seed provided to the flow_from_directory function to ensure aligned data flow
            batch_size - number of images to import at a time
        Output: Decoded RGB image (height x width x 3) 
    '''

    train_image_generator = train_frames_datagen.flow_from_directory(
    DATA_PATH + 'train_frames/',
    batch_size = batch_size, seed = seed,target_size=(144,144),shuffle=True)

    train_mask_generator = train_masks_datagen.flow_from_directory(
    DATA_PATH + 'train_masks/',
    batch_size = batch_size, seed = seed,target_size=(144,144),shuffle=True)

    while True:
        X1i = train_image_generator.next()
        X2i = train_mask_generator.next()
        
        # cv2.imshow("Image 1: ",X1i[0][2,:,:,:].astype('uint8'))
        # cv2.imshow("Test 1:",X2i[0][2,:,:,:].astype('uint8'))
        # cv2.waitKey(0)

        mask_encoded = [rgb_to_onehot(X2i[0][x,:,:,:], id2code) for x in range(X2i[0].shape[0])]
        #print(mask_encoded.shape)
        yield X1i[0], np.asarray(mask_encoded)

def ValAugmentGenerator(seed = 909, batch_size = 16):
    '''Validation Image data generator
        Inputs: 
            seed - seed provided to the flow_from_directory function to ensure aligned data flow
            batch_size - number of images to import at a time
        Output: Decoded RGB image (height x width x 3) 
    '''
    val_image_generator = val_frames_datagen.flow_from_directory(
    DATA_PATH + 'val_frames/',
    batch_size = batch_size, seed = seed,target_size=(144,144),shuffle=False)


    val_mask_generator = val_masks_datagen.flow_from_directory(
    DATA_PATH + 'val_masks/',
    batch_size = batch_size, seed = seed,target_size=(144,144),shuffle=False)


    while True:
        X1i = val_image_generator.next()
        X2i = val_mask_generator.next()
        
        #print(X1i[0])
        #One hot encoding RGB images
        mask_encoded = [rgb_to_onehot(X2i[0][x,:,:,:], id2code) for x in range(X2i[0].shape[0])]
        
        yield X1i[0], np.asarray(mask_encoded)

if __name__ == '__main__':
        
    DATA_PATH = '/home/dylan/Desktop/Goodgame_Training/data/Data_4_class_Non_Lib/'
    
    # Seed defined for aligning images and their masks
    
    batch_size_train = 32 # Batch size
    
    batch_size_val = 32
  
    num_epochs = 500 #Number of epochs

    train_numbers = 2665 # Modify here
    
    val_numbers = 666 # Modify here
    
    #print(img_dir+"label_colors.txt")

    # Cau truc mang PSPNet

    # model = Unet(backbone_name='mobilenetv2', 
    #                 input_shape=(144, 144, 3), 
    #                 classes=9,
    #                 activation='softmax', 
    #                 weights=None, 
    #                 encoder_weights='imagenet', 
    #                 encoder_freeze=True, 
    #                 encoder_features='default', 
    #                 decoder_block_type='upsampling', 
    #                 decoder_filters=(256, 144, 64, 32, 16), 
    #                 decoder_use_batchnorm=True)
    
    # model = Linknet(backbone_name='mobilenetv2',
    #                 input_shape=(144, 144, 3),
    #                 classes=1, activation='softmax',
    #                 weights=None, encoder_weights='imagenet',
    #                 encoder_freeze=True,
    #                 encoder_features='default',
    #                 decoder_block_type='upsampling',
    #                 decoder_filters=(144, 64, 32, 16, 8),
    #                 decoder_use_batchnorm=True)


    model = PSPNet(backbone_name='mobilenetv2',
                       input_shape=(144, 144, 3),
                       classes=4,
                       activation='softmax',
                       weights=None,
                       encoder_weights='imagenet',
                       encoder_freeze=True,
                       downsample_factor=4,
                       psp_conv_filters=512,
                       psp_pooling_type='avg',
                       psp_use_batchnorm=True,
                       psp_dropout=None)
   
    # model = FPN(backbone_name='efficientnetb7', 
    #                 input_shape=(144, 144, 3), 
    #                 classes=9, 
    #                 activation='softmax', 
    #                 weights=None, 
    #                 encoder_weights='imagenet',
    #                 encoder_freeze=True, 
    #                 encoder_features='default', 
    #                 pyramid_block_filters=256, 
    #                 pyramid_use_batchnorm=True, 
    #                 pyramid_aggregation='concat', 
    #                 pyramid_dropout=None)

    model.summary()

    
    label_codes, label_names = zip(*[parse_code(l) for l in open(DATA_PATH+"label_colors.txt")])
    label_codes, label_names = list(label_codes), list(label_names)
    #label_codes[:5], label_names[:5]

    print(label_codes, label_names)
    
    '''
    ### Create useful label and code conversion dictionaries
    These will be used for:
    One hot encoding the mask labels for model training
    Decoding the predicted labels for interpretation and visualization
    '''

    code2id = {v:k for k,v in enumerate(label_codes)}
    id2code = {k:v for k,v in enumerate(label_codes)}

    name2id = {v:k for k,v in enumerate(label_names)}
    id2name = {k:v for k,v in enumerate(label_names)}

    print(id2code,id2name)

    # Normalizing only frame images, since masks contain label info
    # For Aug data
    # self.data_gen_args = dict(rotation_range=0.2,
    #                           width_shift_range=0.05,
    #                           height_shift_range=0.05,
    #                           shear_range=0.05,
    #                           zoom_range=0.05,
    #                           vertical_flip=True,
    #                           horizontal_flip=True,
    #                           fill_mode='nearest')

    data_gen_args = dict( rotation_range=0.2,
                          width_shift_range=0.05,
                          height_shift_range=0.05,
                          shear_range=0.05,
                          zoom_range=0.05,
                          #vertical_flip=True, #can than voi bien bao
                          #horizontal_flip=True,
                          #shuffle=True,
                          #fill_mode='nearest',
                          rescale=1./255)

    mask_gen_args = dict( rotation_range=0.2,
                          width_shift_range=0.05,
                          height_shift_range=0.05,
                          shear_range=0.05,
                          zoom_range=0.05,
                          #vertical_flip=True, #can than voi bien bao
                          #horizontal_flip=True,
                          #shuffle=True,
                          rescale=1./255,
                          fill_mode='nearest')

    data_gen_args_val = dict(rescale=1./255)
    mask_gen_args_val = dict(rescale=1./255)
    
    train_frames_datagen = ImageDataGenerator(**data_gen_args)
    train_masks_datagen = ImageDataGenerator(**mask_gen_args)
    val_frames_datagen = ImageDataGenerator(**data_gen_args_val)
    val_masks_datagen = ImageDataGenerator(**mask_gen_args_val)

    # Call Model cua mot so cau truc mang: Unet, Unet_densenet, bisenet
    #model = unet(num_class = 3) #If want to import unet model
    # model = unet_densenet((144,144,3),num_class=9) # Call denseNet model
    # model = bisenet(144,144,num_class=3) # Call Bisenet Mode

    #Load Pre Model If you had:
    #model.load_weights('/home/dylan/Desktop/Goodgame_Training/model/ModelHaiAnh/2000Data_Lan3_unet_epoch-062_loss-0.4264_val_loss-0.3778.h5')
    
    #We called model before 
    adam = Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)

    #List labels map 
    #0-road, 1-line, 2-background, 3-stop, 4-left, 5-No right, 6-No left, 7-Right, 8-Straight

    # class_weights_dice = [0.3665,0.0279,0.5578,0.0082,0.0078,0.0076,0.0079,0.0080,0.0078] 
    class_weights_dice = [0.3664,0.0280,0.5578,0.0476]

    # class_indexes_dice = [0,1,2,3,4,5,6,7,8]
    class_indexes_dice = [0,1,2,3]
    # class_indexes_focal = [0,1,3,4,5,6,7,8]

    # --------------------------------------------------------------------
    #List of the loss value
    # jaccard_loss = JaccardLoss(class_weights=class_weights_dice, class_indexes=class_indexs_dice)
    dice_loss = DiceLoss(beta=0.5,class_weights=class_weights_dice, class_indexes=class_indexes_dice) #khop anh 

    # binary_focal_loss = BinaryFocalLoss()
    categorical_focal_loss = CategoricalFocalLoss(alpha=0.25, gamma=2)

    # binary_crossentropy = BinaryCELoss()
    #categorical_crossentropy = CategoricalCELoss(class_weights=class_weights_dice,class_indexes=class_indexs_dice) # 

    # loss combinations
    # bce_dice_loss = binary_crossentropy + dice_loss
    # bce_jaccard_loss = binary_crossentropy + jaccard_loss

    # cce_dice_loss = categorical_crossentropy + dice_loss
    # cce_jaccard_loss = categorical_crossentropy + jaccard_loss

    # binary_focal_dice_loss = binary_focal_loss + dice_loss
    # binary_focal_jaccard_loss = binary_focal_loss + jaccard_loss

    categorical_focal_dice_loss = categorical_focal_loss + dice_loss
    # categorical_focal_jaccard_loss = categorical_focal_loss + jaccard_loss 
    #------------------------------------------------------------------------

    #Total Loss
    # total_loss = categorical_focal_dice_loss

    metrics = [sm.metrics.IOUScore(threshold=0.5), sm.metrics.FScore(threshold=0.5), Precision(threshold=0.5), Recall(threshold=0.5)]
    # metrics = [sm.metrics.FScore(threshold=0.5)]
    #metrics = [sm.metrics.IOUScore(threshold=0.5)]
    
    #model.compile(optimizer="adam", loss = 'categorical_crossentropy', metrics = metrics)
    model.compile(adam, loss = categorical_focal_dice_loss, metrics = metrics)
    #model.compile(optimizer='adam', loss="categorical_crossentropy", metrics=[tversky_loss,dice_coef,'accuracy'])

    #model.load_weights("goodgame.h5") # If you have a pretrained model you can load it.

    #tb = TensorBoard(log_dir='logs', write_graph=True)
    
    model_checkpoint = ModelCheckpoint(mode='auto', filepath='./model/Model_4_class_Test_1_epoch-{epoch:03d}_loss-{loss:.4f}_val_loss-{val_loss:.4f}.h5', 
                         monitor='val_loss', 
                         save_best_only='True', 
                         save_weights_only='True', 
                         period=1,
                         verbose=1)
    early_stopping = EarlyStopping(monitor='val_loss',
                                   min_delta=0.0,
                                   patience=20,
                                   verbose=1)
    reduce_learning_rate = ReduceLROnPlateau(monitor='val_loss',
                                             factor=0.2,
                                             patience=10,
                                             verbose=1,
                                             epsilon=0.001,
                                             cooldown=0,
                                             min_lr=0.00001)
    callbacks = [model_checkpoint, early_stopping ,reduce_learning_rate]
    # callbacks = [
    #     ModelCheckpoint('./model/gg_seg-{epoch:03d}_loss-{loss:.4f}_val_loss-{val_loss:.4f}.h5',save_weights_only=True, save_best_only=True, mode='min'),
    #     ReduceLROnPlateau(),
    # ]

    steps_per_epoch = np.ceil(float(train_numbers) / float(batch_size_train))
    validation_steps = np.ceil(float(val_numbers) / float(batch_size_val))
    result = model.fit_generator(TrainAugmentGenerator(batch_size=batch_size_train), steps_per_epoch=steps_per_epoch,
                    validation_data = ValAugmentGenerator(batch_size=batch_size_val), 
                    validation_steps = validation_steps, epochs=num_epochs, callbacks=callbacks)


    #print(result.history)
    # Get actual number of epochs model was trained for
    # N = len(result.history['loss'])

    # #Plot the model evaluation history
    # plt.style.use("ggplot")
    # fig = plt.figure(figsize=(20,8))

    # fig.add_subplot(1,2,1)
    # plt.title("Training Loss")
    # plt.plot(np.arange(0, N), result.history["loss"], label="train_loss")
    # plt.plot(np.arange(0, N), result.history["val_loss"], label="val_loss")
    # plt.ylim(0, 1)

    # fig.add_subplot(1,2,2)
    # plt.title("Training Accuracy")
    # plt.plot(np.arange(0, N), result.history["acc"], label="train_accuracy")
    # plt.plot(np.arange(0, N), result.history["val_acc"], label="val_accuracy")
    # plt.ylim(0, 1)

    # plt.xlabel("Epoch #")
    # plt.ylabel("Loss/Accuracy")
    # plt.legend(loc="lower left")
    # plt.show()

    # #plt.savefig('./log/log.png')
    # fig.savefig('./log/log.png')
    
    ''' 
    Predict test images
    '''
    #training_gen = TrainAugmentGenerator()
    testing_gen = ValAugmentGenerator(batch_size=180) # Em chinh so anh tap test o day nhe

    batch_img,batch_mask = next(testing_gen)
    pred_all= model.predict(batch_img)

    #Test for multi images
    for i in range(0,np.shape(pred_all)[0]):
        
        fig = plt.figure(figsize=(20,8))
        
        ax1 = fig.add_subplot(1,3,1)
        ax1.imshow(batch_img[i])
        ax1.title.set_text('Actual frame')
        ax1.grid(b=None)
        
        
        ax2 = fig.add_subplot(1,3,2)
        ax2.set_title('Ground truth labels')
        ax2.imshow(onehot_to_rgb(batch_mask[i],id2code))
        ax2.grid(b=None)
        
        ax3 = fig.add_subplot(1,3,3)
        ax3.set_title('Predicted labels')
        ax3.imshow(onehot_to_rgb(pred_all[i],id2code))
        ax3.grid(b=None)
        url = './predict/predict_' + str(i)  + '.png' # Sua duong dan tai day, moi lan train thi cho vao mot folder khac.
        plt.savefig(url,dpi=fig.dpi)
        # plt.show() # Neu khong muon xem de no tu save thi comment dong nay lai

    # Test for one image to real-time
    # img = cv2.imread('/content/data/CDS/train_frames/train/12490.jpg')
    # cv2_imshow(img)
    # img = cv2.resize(img,(144,144))
    # img = (img[...,::-1].astype(np.float32)) / 255.0
    # img = np.reshape(img, (1, 144, 144, 3))
    # #print(img.shape)
    # predict_one = model.predict(img)
    # image = onehot_to_rgb(predict_one[0],id2code)
    # #image *= 255
    # cv2_imshow(image) # If you use local machine cv2.imshow instead of cv2_imshow
    # #cv2.waitKey(1) # Uncomment if you use local machine 

    # fig = plt.figure(figsize=(20,8))

    # ax3 = fig.add_subplot(1,3,3)
    # ax3.set_title('Predicted labels')
    # ax3.imshow(onehot_to_rgb(predict_one[0],id2code))

    # plt.show()

    #Mo comment nay ra la se predict anh ma em muon dua tren model da tran
