# example of zoom image augmentation
from numpy import expand_dims
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import ImageDataGenerator
from matplotlib import pyplot
import cv2





# load the image
img = load_img('/home/dylan/Desktop/Goodgame_Training/data/Data_9_class_All/val_frames/val/Anh_pre_(1800).png') # Thay anh vao di
img2 = load_img('/home/dylan/Desktop/Goodgame_Training/data/Data_9_class_All/val_masks/val/Anh_pre_(1800).png')
# convert to numpy array
data = img_to_array(img)
data2 = img_to_array(img2)
# expand dimension to one sample
samples = expand_dims(data, 0)
samples2 = expand_dims(data2, 0)
# create image data augm
# 
# entation generator
datagen = ImageDataGenerator(rotation_range=1,
							width_shift_range=1.0,
							height_shift_range=1.0,
							fill_mode="nearest",
							)
# datagen2 = ImageDataGenerator(zoom_range=0.1,
# 							rotation_range=1,
# 							width_shift_range=1.0,
# 							height_shift_range=1.0,
# 							fill_mode="nearest",
# 							)

# data_gen_args = dict(
#                         width_shift_range=0.05,
#                         height_shift_range=0.05,
#                         shear_range=0.05, 
#                         zoom_range=0.05,
#                         brightness_range=[0.2,1.6], 
#                         fill_mode='nearest',
#                         rescale=1./255)

# prepare iterator
it = datagen.flow(samples, batch_size=1, save_to_dir='/home/dylan/Desktop/Data_Gen/Frame',seed=60)
it2 = datagen.flow(samples2, batch_size=1, save_to_dir='/home/dylan/Desktop/Data_Gen/Mask',seed=0)
print("Len of IT",len(it))
print("Len of IT2",len(it2))
# generate samples and plot
for i in range(9):
	# define subplot
	pyplot.subplot(330 + 1 + i)
	# generate batch of images
	batch = it.next()
	# convert to unsigned integers for viewing
	image = batch[0].astype('uint8')
	# cv2.imshow("Image",image)
	# plot raw pixel data
	# pyplot.imshow(image)
# show the figure
# pyplot.show()

for i in range(9):
	# define subplot
	pyplot.subplot(330 + 1 + i)
	# generate batch of images
	batch2 = it2.next()
	# convert to unsigned integers for viewing
	image2 = batch2[0]#.astype('uint8')
	# plot raw pixel data
	cv2.imshow("Frame",image2)
	pyplot.imshow(image2)
# show the figure
pyplot.show()
