import numpy as np
import matplotlib.pyplot as plt
#%matplotlib inline
import os
import random
import re
from PIL import Image


import numpy as np
import matplotlib.pyplot as plt
from pylab import *
import os
import sys
from tensorflow.keras.models import Model
from tensorflow.keras.regularizers import l2
import tensorflow.keras.backend as K
import tensorflow as tf
from keras.optimizers import Adam
from tensorflow.keras.layers import Convolution2D, ZeroPadding2D, MaxPooling2D, Cropping2D, Conv2D
from tensorflow.keras.layers import Input, Add, Dropout, Permute, add
from tensorflow.compat.v1.layers import conv2d_transpose
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping, ReduceLROnPlateau

from denseunet.unet_densenet import unet_densenet # import denseunet
from bisenet.bisenet import bisenet # Import bisenet model
import cv2
import segmentation_models as sm
from segmentation_models import PSPNet,FPN, Unet
from segmentation_models.metrics import IOUScore
from segmentation_models.losses import JaccardLoss,CategoricalFocalLoss,DiceLoss,CategoricalCELoss
#from keras_radam import RAdam

'''
In our setup, we:
- created a data/ folder
- created CDS folder which contains all folders
- created 4 folders train_frames train_masks val_frames val_masks
- created train and val subfolders inside 4 folders
- put the pictures in data/CDS/train_frames/train/
- put the label pictures in data/train_masks/train/
In summary, this is our directory structure:
```
    data/
        CDS/
            label_colors.txt
            train_frames/
                train/
                    frame1.png # .png format
            train_masks/
                train/
                    frame1_L.png # _L.png format
            val_frames/
                val/
                    frame2.png # .png format
            val_masks/
                val/
                    frame2_L.png # _L.png format
```
'''

#Function to parse the file "label_colors.txt" which contains the class definitions

def parse_code(l):
    '''Function to parse lines in a text file, returns separated elements (label codes and names in this case)
    '''
    if len(l.strip().split("\t")) == 2:
        a, b = l.strip().split("\t") 
        return tuple(int(i) for i in a.split(' ')), b
    
def rgb_to_onehot(rgb_image, colormap):
    '''Function to one hot encode RGB mask labels
        Inputs: 
            rgb_image - image matrix (eg. 144 x 144 x 3 dimension numpy ndarray)
            colormap - dictionary of color to label id
        Output: One hot encoded image of dimensions (height x width x num_classes) where num_classes = len(colormap)
    '''
    #print("RGB: ",rgb_image.shape)
    num_classes = len(colormap)
    shape = rgb_image.shape[:2]+(num_classes,)
    encoded_image = np.zeros( shape, dtype=np.int8 )
    for i, cls in enumerate(colormap):
        encoded_image[:,:,i] = np.all(rgb_image.reshape( (-1,3) ) == colormap[i], axis=1).reshape(shape[:2])
    #print(encoded_image.shape)
    return encoded_image


def onehot_to_rgb(onehot, colormap):
    '''Function to decode encoded mask labels
        Inputs: 
            onehot - one hot encoded image matrix (height x width x num_classes)
            colormap - dictionary of color to label id
        Output: Decoded RGB image (height x width x 3) 
    '''
    single_layer = np.argmax(onehot, axis=-1)
    output = np.zeros( onehot.shape[:2]+(3,) )

    # o day em tao ra mot matrix 144x144 (anh nhi phan kich thuoc 144x144)
    #for 9 class: 0-road, 1-line, 2-background, 3-stop, 4-left, 5-No right, 6-No left, 7-Right, 8-Straight

    # stop = np.zeros((144,144))
    # stop[single_layer==3] = 255 
    # cv2.imshow("stop",stop)

    # left = np.zeros((144,144))
    # left[single_layer==4] = 255
    # cv2.imshow("left",left)

    # right = np.zeros((144,144))
    # right[single_layer==7] = 255
    # cv2.imshow("right",right)

    # noleft = np.zeros((144,144))
    # noleft[single_layer==6] = 255
    # cv2.imshow("noleft",noleft)

    # noright = np.zeros((144,144))
    # noright[single_layer==5] = 255
    # cv2.imshow("noright",noright)

    # straight = np.zeros((144,144))
    # straight[single_layer==8] = 255
    # cv2.imshow("straight",straight)


    for k in colormap.keys():
        output[single_layer==k] = colormap[k]
    return np.uint8(output)

def increase_brightness(img, value=30):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    lim = 255 - value
    v[v > lim] = 255
    v[v <= lim] += value

    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    return img

def adjust_gamma(image, gamma=1.0):
	# build a lookup table mapping the pixel values [0, 255] to
	# their adjusted gamma values
	invGamma = 1.0 / gamma
	table = np.array([((i / 255.0) ** invGamma) * 255
		for i in np.arange(0, 256)]).astype("uint8")
	# apply gamma correction using the lookup table
	return cv2.LUT(image, table)

if __name__ == '__main__':
    
    img_dir = '/home/dylan/Desktop/Goodgame_Training/data/Data_4_class_Non_Lib/'
    
    DATA_PATH = '/home/dylan/Desktop/Goodgame_Training/data/Data_4_class_Non_Lib/'
    



    label_codes, label_names = zip(*[parse_code(l) for l in open(img_dir+"label_colors.txt")])
    label_codes, label_names = list(label_codes), list(label_names)
    #label_codes[:5], label_names[:5]

    print(label_codes, label_names)
    
    '''
    ### Create useful label and code conversion dictionaries
    These will be used for:
    One hot encoding the mask labels for model training
    Decoding the predicted labels for interpretation and visualization
    '''

    code2id = {v:k for k,v in enumerate(label_codes)}
    id2code = {k:v for k,v in enumerate(label_codes)}

    name2id = {v:k for k,v in enumerate(label_names)}
    id2name = {k:v for k,v in enumerate(label_names)}

    print(id2code,id2name)

    # Call Model
    #model = unet(num_class = 3) #If want to import unet model

    #model = unet_densenet((144,144,3),num_class=9) # Call denseNet model

    # model = Unet(backbone_name='mobilenetv2', 
    #             input_shape=(144, 144, 3), 
    #             classes=9,
    #             activation='softmax', 
    #             weights=None, 
    #             encoder_weights='imagenet', 
    #             encoder_freeze=True, 
    #             encoder_features='default', 
    #             decoder_block_type='upsampling', 
    #             decoder_filters=(256, 144, 64, 32, 16), 
    #             decoder_use_batchnorm=True)

    model = PSPNet(backbone_name='mobilenetv2',
                       input_shape=(144, 144, 3),
                       classes=4,
                       activation='softmax',
                       weights=None,
                       encoder_weights='imagenet',
                       encoder_freeze=True,
                       downsample_factor=4,
                       psp_conv_filters=512, 
                       psp_pooling_type='avg',
                       psp_use_batchnorm=True,
                       psp_dropout=None)
    


    model.load_weights('./model/Model_4_class_Test_1_epoch-175_loss-0.7673_val_loss-0.7632.h5')

    #Test for one image to real-time
    # img = cv2.imread('/home/dylan/Desktop/Goodgame_Training/data/Data_Test_Model/Anh_pre_(25).png') 
    # cv2.imshow('Raw image',img)
    # img = adjust_gamma(img,1.3)
    # img = cv2.resize(img,(144,144))
    # img = (img[...,::-1].astype(np.float32)) / 255.0
    # img = np.reshape(img, (1, 144, 144, 3))
    # predict_one = model.predict(img)
    # image = onehot_to_rgb(predict_one[0],id2code)
    # image = cv2.resize(image,(640,480))
    # cv2.imwrite('/home/dylan/image.png',image)
    # cv2.imshow('Predict image',image) # If you use local machine cv2.imshow instead of cv2_imshow
    # cv2.waitKey(0) # Uncomment if you use local machine 

    # Test for one video to real-time   
    # Them dia chi video
    cap = cv2.VideoCapture('/home/dylan/Desktop/Goodgame_Training/video/video/Video_Test_Model_Vovinam.avi')
    # cap = cv2.VideoCapture(0)
    i = 0
    directory = '/home/dylan/Desktop/Goodgame_Training/video/video_frame_pre'
    os.chdir(directory)

    # # Define the codec and create VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter('/home/dylan/Desktop/Goodgame_Training/video/output.avi',fourcc, 30.0, (144*2,144))
    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()
        # cv2.imshow('Raw frame',frame)
        # Xu ly video here
        frame = adjust_gamma(frame, gamma=1.3)
        # frame = cv2.fastNlMeansDenoisingColored(frame,None,10,10,7,21)
        # cv2.imshow("frame", frame)
        img = cv2.resize(frame,(144,144))
        img = (img[...,::-1].astype(np.float32)) / 255.0
        img = np.reshape(img, (1, 144, 144, 3))
        predict_one = model.predict(img)

        frame_real = cv2.resize(frame,(144,144))
        frame_pre = onehot_to_rgb(predict_one[0],id2code)
        
        frame_total = np.concatenate((frame_real, frame_pre), axis=1)
        frame_total = cv2.resize(frame_total,(512*2,512))

        #GG
        # cv2.imshow("Test",frame_total)
        #Save video
        out.write(frame_total)

        #Save frame
        # if(i%5==0):
        file_name = 'Anh_pre_('+str(i)+').png'
        cv2.imwrite(file_name, frame_total)       
        i=i+1

        # Display the resulting frame
        #cv2.imshow('frame after pre',frame)
        # if cv2.waitKey(1) & 0xFF == ord('q'):
        #     break

# When everything done, release the capture
# cap.release()

    # fig = plt.figure(figsize=(20,8))

    # ax3 = fig.add_subplot(1,3,3)
    # ax3.set_title('Predicted labels')
    # ax3.imshow(onehot_to_rgb(predict_one[0],id2code))

    # plt.show()

    #Mo comment nay ra la se predict anh ma em muon dua tren model da tran
