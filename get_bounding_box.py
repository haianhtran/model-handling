import cv2
import os
import numba
from numba import jit
import numpy as np

def parse_code(l):
    '''Function to parse lines in a text file, returns separated elements (label codes and names in this case)
    '''
    if len(l.strip().split("\t")) == 2:
        a, b = l.strip().split("\t")
        return tuple(int(i) for i in a.split(' ')), b
    else:
        a, b, c = l.strip().split("\t")
        return tuple(int(i) for i in a.split(' ')), c

#Function get centroid
@jit(nopython=True,fastmath=True)
def getCentroid(image):
    count = 0
    centroid_x = 0
    centroid_y = 0
    for i in range(0,480): # We will compute based on matrix 100x128 (rows * columns)
        for j in range(0,640):
            b,g,r = image[i][j]
            # if (r,g,b) == (64,0,128): #Stop
            # if (r,g,b) == (128,128,128): #Left
            # if (r,g,b) == (64,0,0): #Right
            # if (r,g,b) == (64,128,0): #NoLeft
            # if (r,g,b) == ((192,128,0)): #NoRight
            if (r,g,b) == (192,0,0): #Straight
                count+=1
                centroid_y += i
                centroid_x += j
            # if image[i][j] == (64,0,128): #Class stop
            #     count += 1
            #     centroid_y += i
            #     centroid_x += j
    if centroid_x != 0 or centroid_y != 0 or count != 0:
        print(count)
        centroid_x = centroid_x / count
        centroid_y = centroid_y / count
    print(centroid_y)
    return int(centroid_x),int(centroid_y), count

#Function creat bounding box pre image
def creatBox(masks_folder, frame_folder):
    images_list = os.listdir(masks_folder)
    for i in range(len(images_list)):
        #Get pre image to procceing in dir
        print("Processing---------",i,"/",len(images_list))
        image = cv2.imread(masks_folder + '/' + images_list[i])
        image_frame = cv2.imread(frame_folder + '/' + images_list[i])

        # image = cv2.resize(image,(144,144))
        # image_frame = cv2.resize(image_frame,(144,144))

        # cv2.imshow('Raw frame',image)
        #Get centroid
        centroid_x, centroid_y, count = getCentroid(image)
        if centroid_x == 0 or centroid_y == 0: 
            print("Khong co bien!")
        else:
            # cv2.circle(image_frame,(centroid_x,centroid_y), 5, (0,0,255), -1)
            # cv2.imshow('Test frame',image_frame)
            try:
                if 10000 > count > 8000:
                    image_cut = image_frame[centroid_y-80:centroid_y+80,centroid_x-80:centroid_x+80]
                    cv2.imwrite('/home/dylan/Desktop/Goodgame_Training/tools/Data_CNN/Straight/'+images_list[i],image_cut)
                if 8000 > count > 6000:
                    image_cut = image_frame[centroid_y-65:centroid_y+65,centroid_x-65:centroid_x+65]
                    cv2.imwrite('/home/dylan/Desktop/Goodgame_Training/tools/Data_CNN/Straight/'+images_list[i],image_cut)
                if 6000 > count > 4000:
                    image_cut = image_frame[centroid_y-50:centroid_y+50,centroid_x-50:centroid_x+50]
                    cv2.imwrite('/home/dylan/Desktop/Goodgame_Training/tools/Data_CNN/Straight/'+images_list[i],image_cut)
                if 4000 > count > 2000:
                    image_cut = image_frame[centroid_y-40:centroid_y+40,centroid_x-40:centroid_x+40]
                    cv2.imwrite('/home/dylan/Desktop/Goodgame_Training/tools/Data_CNN/Straight/'+images_list[i],image_cut)
                if 2000 > count > 1000:
                    image_cut = image_frame[centroid_y-30:centroid_y+30,centroid_x-30:centroid_x+30]
                    cv2.imwrite('/home/dylan/Desktop/Goodgame_Training/tools/Data_CNN/Straight/'+images_list[i],image_cut) 
                if 1000 > count > 500:
                    image_cut = image_frame[centroid_y-20:centroid_y+20,centroid_x-20:centroid_x+20]
                    cv2.imwrite('/home/dylan/Desktop/Goodgame_Training/tools/Data_CNN/Straight/'+images_list[i],image_cut)     
            except:
                pass

if __name__ == "__main__":

    img_dir = '/home/dylan/Desktop/Goodgame_Training/data/Data_9_class_All/total_masks/train/'
    
    DATA_PATH = '/home/dylan/Desktop/Goodgame_Training/data/Data_9_class_All/'
    
    img_dir_frame = '/home/dylan/Desktop/Goodgame_Training/data/Data_9_class_All/total_frames/train'

    creatBox(img_dir,img_dir_frame)