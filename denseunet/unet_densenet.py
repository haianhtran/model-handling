from denseunet.unet_block import dense_pool_block
from denseunet.unet_block import Upsample2D_block
from denseunet.unet_block import Transpose2D_block
from keras.layers import Input, Conv2D, UpSampling2D
from keras.models import Model
from keras.optimizers import *

IMG_SIZE = 128

def unet_densenet(input_size,num_class=2):
    inputs = Input(input_size)
    # block1 64
    block1 = dense_pool_block(8, 3, stage=1)(inputs)
    # block2 128
    block2 = dense_pool_block(16, 3, stage=2)(block1)
    # block3 256
    block3 = dense_pool_block(32, 3, stage=3)(block2)
    # block4 512
    block4 = dense_pool_block(64, 3, stage=4)(block3)
    # block5 1024
    block5 = dense_pool_block(128, 3, stage=5)(block4)
    # up1 512
    up1 = Upsample2D_block(64, stage=1, batchnorm=True, skip=block4)(block5)
    # up2 256
    up2 = Upsample2D_block(32, stage=2, batchnorm=True, skip=block3)(up1)
    # up2 128
    up3 = Upsample2D_block(16, stage=3, batchnorm=True, skip=block2)(up2)
    # up3 64
    up4 = Upsample2D_block(8, stage=4, batchnorm=True, skip=block1)(up3)
    
    up5 = Conv2D(8, 2, activation='relu', padding='same', kernel_initializer='he_normal')(
        UpSampling2D(size=(2, 2))(up4))
    # conv = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(up4)
    # conv = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv)
    conv = Conv2D(num_class, 3, activation='relu', padding='same', kernel_initializer='he_normal')(up5)
    if num_class == 2:
        conv10 = Conv2D(1, 1, activation='sigmoid')(conv)
        loss_function = 'binary_crossentropy'
    else:
        conv10 = Conv2D(num_class, 1, activation='softmax')(conv)
        loss_function = 'categorical_crossentropy'
    model = Model(input=inputs, output=conv10)

    model.compile(optimizer=Adam(lr=1e-4), loss=loss_function, metrics=["accuracy"])
    model.summary()

    return model

#model = unet_densenet((128,128,3),num_class=3)
