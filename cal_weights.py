import cv2
import os
import statistics
def calc_weights(masks_folder):
    """Calculate class weights according to classes distribution in a dataset"""
    images_list = os.listdir(masks_folder)
    print(len(images_list))
    class_1_numbers = []
    for i in range(len(images_list)):
        mask = cv2.imread(masks_folder + '/' + images_list[i], cv2.IMREAD_GRAYSCALE) / 255.
        class_1_numbers.append(cv2.countNonZero(mask))
    print(len(class_1_numbers))
    class_1_total = int(statistics.median(class_1_numbers))
    class_0_total = int(IMAGE_WIDTH**2 - class_1_total)
    class_1_weight = 1. # Maximum value to minority class
    class_0_weight = class_1_total / class_0_total # Proportional value to majority class for classes balance
    return [class_0_weight, class_1_weight]
IMAGE_WIDTH = 640
class_weights = calc_weights('/home/dylan/Desktop/Goodgame_Training/data/Eyeball_5000_9class_AllMap_and_Lib/train_masks/train')
print(class_weights) # folder mask train. 